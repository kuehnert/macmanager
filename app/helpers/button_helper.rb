module ButtonHelper
  def icon_link(path, options = {})
		options.reverse_merge! class: '', size: '', title: 'Klick!', icon: 'play', remote: false
		link_params = { class: "#{options[:class]} #{options[:size]}", remote: options[:remote] }
    link_params[:target] = :new if options[:target_new]
		link_params.merge! options[:link_params] || {}
		
		link_to "<i class='glyphicon glyphicon-#{options[:icon]}'></i> #{options[:title]}".html_safe, path, link_params
  end

  def icon_delete_link(path, options = {})
    options[:link_params] = { method: :delete }
    icon_link(path, options)
  end
  
	def make_button(path, options = {})
		options.reverse_merge! class: 'btn-default', size: '', title: 'Klick!', icon: 'play', remote: false
		link_params = { class: "btn #{options[:class]} #{options[:size]}", remote: options[:remote] }
    link_params[:target] = :new if options[:target_new]
		link_params.merge! options[:link_params] || {}
		
		link_to "<i class='glyphicon glyphicon-#{options[:icon]}'></i> #{options[:title]}".html_safe, path, link_params
	end
	
	def submit_button(options = {})
		options.reverse_merge! class: 'btn-primary', size: '', title: 'Speichern', icon: 'save'
		
		button_tag(type: 'submit', class: "btn #{options[:class]} #{options[:size]}") do
			"<i class='glyphicon glyphicon-#{options[:icon]}'></i> #{options[:title]}".html_safe
		end
	end
	
	def back_button
		make_button(:back, title: 'Zurück', icon: 'circle-arrow-left')
	end

  def download_button(path, options = {})
    options.reverse_merge! title: 'Download', icon: 'download', class: 'btn-info clip-button'
    make_button path, options
  end

  def print_button(path, options = {})
    options.reverse_merge! title: 'Drucken (PDF)', icon: 'print', class: 'btn-success'
    make_button path, options
  end

	def home_button
		make_button(home_path, title: 'Heimseite', icon: 'home')
	end
	
  def index_button(path, options = {})
    options.reverse_merge! title: 'Liste', icon: 'list'
    make_button path, options
  end
  
  def show_button(path, options = {})
    options.reverse_merge! title: 'Anzeigen', icon: 'eye-open'
    make_button path, options
  end

  def help_button(path, options = {})
    options.reverse_merge! title: 'Hilfe', icon: 'question-sign'
    make_button path, options
  end
  
  def markdown_help_button
    help_button "https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet", target_new: true,  title: "Markdown", class: 'btn-info'
  end

	def new_button(path, options = {})
		options.reverse_merge! title: 'Neu', icon: 'plus-sign'
		make_button path, options
	end

	def create_button(path, options = {})
		options.reverse_merge! title: 'Erstellen', icon: 'plus-sign'
		make_button path, options
	end
  
	def cancel_button(path, options = {})
		options.reverse_merge! title: 'Abbrechen', icon: 'arrow-left'
		make_button path, options
	end
	
  def edit_button(path, options = {})
		options.reverse_merge! title: 'Bearbeiten', icon: 'pencil'
		make_button path, options
  end
  
  def delete_button(path, options = {})
		options.reverse_merge! title: 'Löschen', icon: 'trash', class: 'btn-danger', confirm: 'wirklich'
		options[:link_params] = { method: :delete, :data => { :confirm => "Wollen Sie #{options[:confirm]} löschen?" } }
		
    make_button path, options
  end
  
  def email_button(email, options = {})
		options.reverse_merge! title: 'Email', icon: 'envelope'

    if email.present?
      make_button "mailto:#{email}", options
    else
      ''
    end
  end

  # Buttons for tables
  def table_email_button(email, options = {})
		options.reverse_merge! title: '', size: 'btn-xs'
    email_button(email, options)
  end
  
  def table_download_button(path, options = {})
		options.reverse_merge! size: 'btn-xs', title: nil
    
    download_button(path, options)
  end
  
  def table_edit_button(path, options = {})
		options.reverse_merge! size: 'btn-xs', title: nil
    
    edit_button(path, options)
  end
  
  def table_delete_button(path, options = {})
		options.reverse_merge! size: 'btn-xs', title: nil
    
    delete_button(path, options)
  end
end
