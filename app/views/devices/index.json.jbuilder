json.array!(@devices) do |device|
  json.extract! device, :hardware_serial, :room, :number, :resolution_x, :resolution_y, :mac_en, :mac_wifi, :manifest_id, :model
  json.url device_url(device, format: :json)
end
