json.array!(@manifests) do |manifest|
  json.extract! manifest, :name
  json.url manifest_url(manifest, format: :json)
end
