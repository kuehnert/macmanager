class Device < ActiveRecord::Base
  before_validation :correct_fields
  
  belongs_to :manifest

  scope :ordered, -> { includes(:manifest).order('manifests.name, room, number').references(:manifests) }
  
  # validates :hardware_serial, presence: true, uniqueness: true, length: { minimum: 12 }
  # validates :room, presence: true, format: { with: /\AR([0-9]{3})\z/ }
  # validates :number, uniqueness: true, numericality: { only_integer: true }, allow_blank: true
  # validates :resolution_x, numericality: { only_integer: true, greater_than_or_equal_to: 16, less_than_or_equal_to: 8192 }, allow_blank: true
  # validates :resolution_y, numericality: { only_integer: true, greater_than_or_equal_to: 16, less_than_or_equal_to: 8192 }, allow_blank: true
  # validates :mac_en, presence: true, uniqueness: true, format: { with: /\A([0-9A-F]{2}[:-]){5}([0-9A-F]{2})\z/ }
  # validates :mac_wifi, presence: true, uniqueness: true, format: { with: /\A([0-9A-F]{2}[:-]){5}([0-9A-F]{2})\z/ }
  # validates :ip, presence: true, format: { with: /\A10\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/ }
  # validates :manifest, presence: true
  # validates :model, presence: true, inclusion: { within: %w(iMac MacMini) }
  
  def correct_fields
    self.mac_en.upcase! if self.mac_en.present?
    self.mac_wifi.upcase! if self.mac_wifi.present?
    self.manifest = Manifest.find_by(name: 'Common') if self.manifest.blank?
  end
  
  def computer_name
    no_s = self.number.present? ? "-#{number}" : nil
    test = self.manifest.name.include?('Test') ? 'T' : nil
    manifest = self.manifest.name.gsub('Test', '')
    room = self.room.present? ? self.room.gsub('R', '') : self.hardware_serial[-6..-1]
    "#{manifest}#{room}#{no_s}#{test}"
  end
  
  def munki_client_identifier
    manifest.name
  end

  def w
    resolution_x
  end

  def h
    resolution_y
  end
end
