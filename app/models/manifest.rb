# == Schema Information
#
# Table name: manifests
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Manifest < ActiveRecord::Base
  has_many :devices

  # validates :name, presence: true
end
