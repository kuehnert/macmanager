class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_filter :update_sanitized_params, if: :devise_controller?
  before_filter :verified_request?
  before_filter :set_active_item

  protected
    def update_sanitized_params
      devise_parameter_sanitizer.for(:sign_up) << :user_name
    end

    def verified_request?
      if request.content_type == 'application/json'
        true
      else
        super()
      end
    end

    def set_active_item
      @active = nil
    end
end
