class ManifestsController < InheritedResources::Base
	before_action :authenticate_admin!

  protected
    def permitted_params
      params.permit!
    end

    def set_active_item
      @active = :manifests
    end
end
