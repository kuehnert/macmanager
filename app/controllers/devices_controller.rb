class DevicesController < InheritedResources::Base
  before_action :authenticate_admin!, except: [:get_settings, :get_resolution, :create]

  def update
    update! { collection_path }
  end

  def get_settings
    @device = Device.find_by hardware_serial: params[:hardware_serial]
    
    if @device.present?
      @device.update_attribute :last_checkin, Time.now
    end

    render json: @device.as_json(only: [], methods: [:computer_name, :munki_client_identifier, :ip])
  end

  def get_resolution
    @device = Device.find_by hardware_serial: params[:hardware_serial]
    
    if @device.present?
      @device.update_attribute :last_checkin, Time.now
    end

    render json: @device.as_json(only: [], methods: [:w, :h])
  end
  
  protected
    def collection
      @devices ||= end_of_association_chain.ordered
    end
  
    def permitted_params
      params.permit!
    end

    def set_active_item
      @active = :devices
    end
end
