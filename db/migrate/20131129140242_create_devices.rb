class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :hardware_serial
      t.string :room
      t.integer :number
      t.integer :resolution_x
      t.integer :resolution_y
      t.string :mac_en
      t.string :mac_wifi
      t.references :manifest, index: true
      t.string :model

      t.timestamps
    end
    add_index :devices, :hardware_serial, unique: true
  end
end
