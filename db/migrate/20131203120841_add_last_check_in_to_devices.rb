class AddLastCheckInToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :last_checkin, :datetime
  end
end
