class CreateManifests < ActiveRecord::Migration
  def change
    create_table :manifests do |t|
      t.string :name

      t.timestamps
    end
    add_index :manifests, :name, unique: true
  end
end
