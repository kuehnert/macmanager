# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140706114759) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: true do |t|
    t.string   "email",              default: "", null: false
    t.string   "encrypted_password", default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree

  create_table "devices", force: true do |t|
    t.string   "hardware_serial"
    t.string   "room"
    t.integer  "number"
    t.integer  "resolution_x"
    t.integer  "resolution_y"
    t.string   "mac_en"
    t.string   "mac_wifi"
    t.integer  "manifest_id"
    t.string   "model"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "last_checkin"
    t.string   "ip"
  end

  add_index "devices", ["hardware_serial"], name: "index_devices_on_hardware_serial", unique: true, using: :btree
  add_index "devices", ["manifest_id"], name: "index_devices_on_manifest_id", using: :btree

  create_table "manifests", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "manifests", ["name"], name: "index_manifests_on_name", unique: true, using: :btree

end
