set :rails_env,   'production'
set :branch,      'master'

server 'neumann.marienschule-intern.de', user: 'deploy', roles: %w{web app db}
