set :rails_env,   'production'
set :branch,      'master'
set :ssh_options, { forward_agent: true, port: 225 }

server 'marienschule-intern.de', user: 'deploy', roles: %w{web app db}
