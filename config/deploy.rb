lock '3.4.0'

set :application, 'macmanager'
set :repo_url,      'git@bitbucket.org:kuehnert/macmanager.git'
set :deploy_to, -> { "/Library/Server/Web/Data/RailsApps/#{fetch(:application)}" }
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

set :linked_files,  %w{config/database.yml}
set :linked_dirs,   %w{log tmp/pids tmp/cache tmp/sockets public/system}

set :format,        :pretty
set :log_level,     :debug

set :rails_env, 'production'
set :default_env, -> { { 'RAILS_RELATIVE_URL_ROOT' => "/#{fetch(:application)}" } }

set :keep_releases, 5

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart
  after :finishing, :cleanup

  # after :restart, :clear_cache do
  #   on roles(:web), in: :groups, limit: 3, wait: 10 do
  #     # Here we can do anything such as:
  #     # within release_path do
  #     #   execute :rake, 'cache:clear'
  #     # end
  #   end
  # end
end
