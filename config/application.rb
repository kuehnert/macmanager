require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module MacManager
  class Application < Rails::Application
    config.autoload_paths      += %W(#{config.root}/lib)
    config.encoding             = "utf-8"
    config.filter_parameters   += [:password, :password_confirmation]
    config.i18n.default_locale  = :en
    config.time_zone            = 'Berlin'
  end
end
